var request = require('request');
var express = require('express');
var fs = require('fs');
var https = require('https');

var app = express();
var port = 8080;
var hostname = "https://localhost/";

var privateKeyPath = __dirname + "/privatekey.key";
var certificatePath = __dirname + "/certificate.pem";

var privateKey = fs.readFileSync( privateKeyPath );
var certificate = fs.readFileSync( certificatePath );

var server = https.createServer({
    key: privateKey,
    cert: certificate
});

var bots = [];

var botsDir = __dirname + "/bots/enabled";
var botNames = fs.readdirSync(botsDir);
for (let i = 0; i < botNames.length; i++) {
    var bot = require(botsDir + "/" + botNames[i]);
    bots[i] = {
        botName: botNames[i],
        apiKey: bot.apiKey,
        requestHandler: bot.requestHandler
    };
}

for (let i = 0; i < bots.length; i++) {
    var botServerLink = "/" + bots[i].apiKey;
    app.get(botServerLink, bots[i].requestHandler);
    
    var url = "https://api.telegram.org/" + bots[i].apiKey 
        + "/setWebhook?url=" + encodeURIComponent("https://" + botServerLink);
    var formData = {
        certificate: fs.createReadStream(certificatePath)
    };
        
    request({url: url, formData: formData}, function (error, response, body) {
      if (!error && response.statusCode == 200) {
      } else {
        console.log("Network error for bot " + bots[i].botName);
      }
    });
}

server.on('request', app);
server.listen(port);
